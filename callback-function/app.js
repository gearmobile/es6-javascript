function printSome () {
  console.log(`Hello from callback`)
}

function highOrder (callback) {
  console.log(`Start function highOrder`)
  callback() // => here invoke callback function
  console.log(`Stop function hightOrder`)
}

highOrder(printSome)