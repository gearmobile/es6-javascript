const name = `john`

function greeting (param, callback) {
  return `Hello, ${callback(param)}`
}

function upperCase (value) {
  return value.toUpperCase()
}

function makeCap (value) {
  return value.charAt(0).toUpperCase() + value.slice(1)
}

console.log(greeting(name, upperCase))
console.log(greeting(name, makeCap))