class Task {
  constructor (title = '', done = false) {
    this.title = title
    this.done = done
  }
  completeTask (value) {
    this.done = value
  }
}

Task.count = 0

let taskFirst = new Task('First Task', false)
let taskSecond = new Task('Second Task', true)

console.log(typeof Task) // => function
console.log(typeof task) // => object
console.log(taskFirst instanceof Task) // => true

console.log(`Task First: "${taskFirst.done}"`) // => false
taskFirst.completeTask(true)
console.log(`Task First: "${taskFirst.done}"`) // => true

console.log(taskSecond.title) // => 'Second Task'
console.log(taskSecond.done) // => true
