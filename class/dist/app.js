'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Task = function () {
  function Task() {
    var title = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    var done = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

    _classCallCheck(this, Task);

    this.title = title;
    this.done = done;
  }

  _createClass(Task, [{
    key: 'completeTask',
    value: function completeTask(value) {
      this.done = value;
    }
  }]);

  return Task;
}();

Task.count = 0;

var taskFirst = new Task('First Task', false);
var taskSecond = new Task('Second Task', true);

console.log(typeof Task === 'undefined' ? 'undefined' : _typeof(Task)); // => function
console.log(typeof task === 'undefined' ? 'undefined' : _typeof(task)); // => object
console.log(taskFirst instanceof Task); // => true

console.log('Task First: "' + taskFirst.done + '"'); // => false
taskFirst.completeTask(true);
console.log('Task First: "' + taskFirst.done + '"'); // => true

console.log(taskSecond.title); // => 'Second Task'
console.log(taskSecond.done); // => true