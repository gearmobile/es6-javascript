const promise = new Promise((resolve, reject) => {
  reject(`some error happend`) // => что случится, если промис не выполнится
})

promise
  .then(() => console.log(`It is happend sometimes`))
  .catch((data) => console.log(data)) // => всегда будет срабатывать этот метод, так как в объекте promise определен только он