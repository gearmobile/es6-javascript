const promise = new Promise((res, rej) => {
  const random = Math.random().toFixed(1)
  if (random > 0.5) {
    res(random)
  } else {
    rej(random)
  }
})

promise
  .then(data => console.info(`Success result is ${data}`))
  .catch(data => console.error(`Error result is ${data}`))