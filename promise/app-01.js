const sample = new Promise((resolve, reject) => {
  resolve([1,2,3,4,5])
})

sample.then(data => console.log(`Promise sample resolve this data - ${data}`))