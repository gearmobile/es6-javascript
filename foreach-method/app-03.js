const fruits = ['apple', 'cherry', 'lemon', 'kiwi', 'mango']

function double (array) {
  let result = ''
  array.forEach((element, index) => {
    if (array.length - 1 !== index) { // => array.length - 1 return index of last item os array
      result += `${element} `
    } else {
      result += `${element}!`
    }
  })
  console.log(result)
}

double(fruits)