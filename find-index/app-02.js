const numbers = [ 1, 7, 13, 41, 25, 11, 2, 4, 6, 33 ]

function findEven (array) {
  return array.findIndex((el, i, array) => {
    return el % 2 === 0 // => return index of the first element that passes condition
  })
}

console.info(findEven(numbers))