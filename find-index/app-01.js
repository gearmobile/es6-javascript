const fruits = [ 'apple', 'cherry', 'lemon', 'kiwi', 'mango' ]
const numbers = [ 1, 2, 3, 4, 5 ]

function checkIndex (array, value) {
  return array.findIndex((el, i, array) => {
    return el === value
  })
}

console.info(checkIndex(numbers, 5))
console.info(checkIndex(fruits, 'kiwi'))