const languages = [ 'C#', 'Java', 'Python', 'Pascal', 'JavaScript', 'HTML', 'XML', 'Dart', 'Go' ]

function findLanguage (array, value) {
  let result = array.findIndex((el, i, array) => {
    return el === value
  })
  return result !== -1 ? `Value ${value} is an array` : `Value ${value} was not found in array`
}

console.log(findLanguage(languages, 'CSS'))
console.log(findLanguage(languages, 'JavaScript'))